#!/bin/bash
#
#  emerge-use-test.sh
#
#  Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

# This script rebuilds a package with different USE-flag combinations
# and optionally rebuilds packages (most often reverse-dependencies)
# afterwards for each combination.

script=`basename ${0}`

help="
Usage: ${script} [OPTIONS...] <package> [AFTER...]
Rebuild a package with different USE-flag combinations and re-emerge
other packages afterwards.

  -u, --use   USE-flags that should be combined [default is all].
  -l, --log   logfile
  -h, --help  display this help and exit"

bold="\e[1m"
normal="\e[0m"

default="\e[39m"
green="\e[92m"
red="\e[91m"

msg() {
    echo -e "${bold}${blue}${script}${default}: ${@}${normal}"
}

die() {
    msg "${red}${@}${default}"
    exit 1
}

if test $# -lt 2
then
    die "too few arguments"
fi

logfile=""
package=""
after=""
use=""

while [[ $# -gt 0 ]]
do
    case "${1}" in
    --log|-l)   shift; logfile="${1}";;
    --log=*)    logfile="${1/--log=/}";;
    -l*)        logfile="${1/-l/}";;
    --use|-u)   shift; use="${use} ${1}";;
    --use=*)    use="${use} ${1/--use=/}";;
    -u*)        use="${use} ${1/-u/}";;
    --help|-h)  echo "${help}"; exit 0;;
    *)
        if [[ -z "${package}" ]]; then
            package="${1}"
        else
            after="${after} ${1}"
        fi
        ;;
    esac
    shift
done

# Do not let the poor runner of this script forget to log!
if [ -z "${logfile}" ]
then
    logfile=$(mktemp "${script}.log.XXXXXXXX")
fi

# convert after and use to arrays
after=(${after})
use=($use)

if [ ${#use[@]} -eq 0 ]
then
    msg "No USE-flags specified assuming all."
    use="$(equery u ${dep} | sed 's/^[+-]//g')"
else
    # Remove commas from use.
    use="${use//,/ }"
fi

msg "USE-flags are"
for u in ${use}
do
    msg "    ${u}"
done

result=""

# Construct a nice header.
for u in ${use[@]}
do
    result="${result} ${u}"
done
result="${result} | ${package}"
for a in ${after[@]}
do
    result="${result} ${a}"
done

# The strings to test for unsatisfied required use and failure.
failstr="Failed to emerge"
requsestr="The following REQUIRED_USE flag constraints are unsatisfied"

# Translation
# 0 = SUCCESS
# 1 = FAILURE
# 2 = REQUIRED_USE
emerge_with_result() {
    if [ $# -ne 1 ]
    then
        die "emerge_with_result: invalid number of arguments"
    fi

    local pkg="${1}"
    local status=0
    local tmpfile=$(mktemp ${script}.XXXXXXXX)

    USE="${USE}" emerge --oneshot --verbose "${pkg}" 2>&1 | tee "${tmpfile}"

    if grep -q "${failstr}" "${tmpfile}"
    then
        status=1
    elif grep -q "${requsestr}" "${tmpfile}"
    then
        status=2
    fi

    rm "${tmpfile}"
    return ${status}
}

status_to_str() {
    if [ $# -ne 1 ]
    then
        die "status_to_str: invalid number of arguments"
    fi

    local status=${1}
    if [ ${status} -eq 0 ]
    then
        echo S
    elif [ ${status} -eq 1 ]
    then
        echo F
    elif [ ${status} -eq 2 ]
    then
        echo R
    else
        die "status_to_str: invalid status"
    fi
}

# USAGE: number_of_spaces
print_spaces() {
    if [ ${#} -ne 1 ]
    then
        die "print_spaces: invalid number of arguments"
    fi

    local n=${1}
    local i=0
    while [ ${i} -lt ${n} ]
    do
        echo -n " "
        i=$(( ${i} + 1 ))
    done

    return 0
}

# USAGE: char string
print_centered_char() {
    if [ $# -ne 2 ]
    then
        die "$0: invalid number of arguments"
    fi

    local char="${1}"
    local string="${2}"
    local len=${#string}
    local quot=$(expr ${len} / 2)
    local rest=$(expr ${len} % 2)

    print_spaces ${quot}
    echo -n "${char}"
    if [ ${rest} -eq 1 ]
    then
        print_spaces ${quot}
    else
        print_spaces $(( ${quot} - 1))
    fi

    return 0
}

# Give USE as an environment variable to this function.
# USAGE: pkgresult afterresult
save_result() {
    local pkgres="${1}"
    shift
    local afterres=(${@})

    # Begin a new line in result.
    result="${result}\n"

    # Parse USE. + means set, - otherwise.
    for flag in ${USE}
    do
        local char="+"
        if [ "${flag:0:1}" = "-" ]
        then
            char="-"
            flag="${flag:1}"
        fi
        result="${result} $(print_centered_char ${char} ${flag})"
    done

    # Add this result.
    result="${result} | $(print_centered_char ${pkgres} ${package})"

    # Parse all afterresults.
    for pair in ${afterres[@]}
    do
        local arr=($(echo "${pair}" | tr : ' '))
        result="${result} $(print_centered_char ${arr[1]} ${arr[0]})"
    done

    return 0
}

recurse() {
    local i=${1}
    shift
    local use=(${@})
    local this=${use[${i}]}

    # Just for nitpick, remove the leading space in USE.
    if [ ${i} -eq 1 ]
    then
        USE="$(echo ${USE} | sed 's/^ //g')"
    fi

    if [ ${i} -eq ${#use[@]} ]
    then
        # Results. Both this (R/S/F) and the after ones (S/F).
        local res=""
        local afterres=()

        msg "Trying USE=\"${USE}\"."
        USE="${USE}" emerge_with_result "${package}"
        res="$(status_to_str ${?})"

        # There is no reason to go on and build all packages in after if
        # the build did not succeed, so split this in two cases.
        if [ "${res}" = "S" ]
        then
            for a in ${after[@]}
            do
                local status
                emerge_with_result ${a}
                afterres+=("${a}:$(status_to_str ${?})")
            done
        else
            for a in ${after[@]}
            do
                afterres+="${a}:U"
            done
        fi

        save_result "${res}" "${afterres[@]}"
    else
        USE="${USE} -${this}" \
            recurse $(expr ${i} + 1) ${use[@]}
        USE="${USE} ${this}" \
            recurse $(expr ${i} + 1) ${use[@]}
    fi

    return 0
}

main() {
    recurse 0 ${use[@]}
    echo -e "${result}" | sed 's/^ //g'
    echo "+ = USE-flag is set"
    echo "- = USE-flag is unset"
    echo "S = Build succeeded"
    echo "F = Build failed"
    echo "R = Unsatisfied REQUIRED_USE constraints"
    echo "U = Skipped unnecessary rebuild"
}

main | tee "${logfile}"

exit 0
